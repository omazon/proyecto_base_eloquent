<?php
require_once "app/bootstrap.php";
$router = new Router();
require 'routes/web.php';

$uri = trim($_SERVER['REQUEST_URI'],'/');

require $router->direct($uri);
